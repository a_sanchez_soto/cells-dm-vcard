import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-dm-vcard-styles.js';
import '@vcard-components/cells-util-behavior-vcard';
import '@vcard-components/cells-http-request-vcard';
/**
This component ...

Example:

```html
<cells-dm-vcard></cells-dm-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsDmVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-dm-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      idDp: { type: String },
      timeout: { type: Number }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.idDp = 'baseDmDp';
    this.timeout = 30000;
  }

  settings(config) {
    if (!config) {
      return;
    };
    if (!config.method) {
      config.method = 'GET';
    }

    if(!config.host){
      config.host = this.extract(window,'AppConfig.services.host','');
    }

    config.onReloadFnRequest = (dmReload) => {
      this.sendRequest(null, dmReload);
    };

    let idRamdom = Math.floor(1 + Math.random() * (999999999));
    let dp = this.shadowRoot.getElementById(this.idDp);
   
    let dpClone = dp.cloneNode(true);
    Object.assign(dpClone, config);
    dpClone.id = `vcard-dm-${idRamdom}`;

    if(typeof dpClone.requiresToken === 'undefined'){
      dpClone.requiresToken = true;
    }

    let parentElement = this.shadowRoot;
    parentElement.appendChild(dpClone);
    return dpClone;
  }

  sendRequest(config, dm) {
    config = config || {};
    if (config.detail) {
      config = config.detail;
    }
    console.log("CellsDmVcard.sendRequest()", config)
    try {
      let dp;
      let parentElement = this.shadowRoot;
      let onSuccess, onError, spinnerShow;
      if (dm) {
        dp = dm;
        onSuccess = dp.onSuccess;
        onError = dm.onError;
        spinnerShow = dm.spinnerShow;
        config.detailNone = dm.detailNone;
      } else {
        dp = this.settings(config);
        onSuccess = config.onSuccess;
        onError = config.onError;
        spinnerShow = config.spinnerShow
      }
      if (typeof spinnerShow !== 'undefined') {
        this.invokeDispatchSpinner(true);
      }

      dp.dataset.intentos = (!dp.dataset.intentos ? 1 : (dp.dataset.intentos = parseInt(dp.dataset.intentos) + 1));
     
      dp.generateRequest().then(
        (response) => { 
          let detail = config.detailNone ? response : { detail: response };
          onSuccess(detail);
          parentElement.removeChild(dp);
          if (typeof spinnerShow !== 'undefined') {
            this.invokeDispatchSpinner(false);
          }
        },
        (error) => {
          this.evaluateErrorDM(error, dp, onError, parentElement);
        });
    } catch (e) {
      console.log("Error CellsCotizaDmvcard.sendRequest", e);
      this.invokeDispatchSpinner(false);
    }
  }

  evaluateErrorDM(error, dp, onError, parentElement) {
    if (onError) { onError(error); }
      parentElement.removeChild(dp);
      this.invokeDispatchSpinner(false);
  }

  invokeDispatchSpinner(value) {
    this.dispatch(this.extract(window,'AppConfig.events.globalSpinner','spinner-global-event'), value);
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-dm-vcard-shared-styles').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <cells-http-request-vcard timeout = "${this.timeout}" id="${this.idDp}"></cells-http-request-vcard>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsDmVcard.is, CellsDmVcard);
